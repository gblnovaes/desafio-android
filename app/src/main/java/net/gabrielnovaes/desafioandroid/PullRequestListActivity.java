package net.gabrielnovaes.desafioandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import net.gabrielnovaes.desafioandroid.adapters.ListPullRequestAdapter;
import net.gabrielnovaes.desafioandroid.models.GithubPullRequestsResponse;
import net.gabrielnovaes.desafioandroid.rest.ApiClient;
import net.gabrielnovaes.desafioandroid.services.GitHubRepositoriesService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequestListActivity extends AppCompatActivity {

    private static final String TAG = "resultado";
    private GitHubRepositoriesService apiService;
    private List<GithubPullRequestsResponse> listPullRequests = new ArrayList<>();
    private String title_repository;
    private String owner_repository;
    private String open_issues;
    private String watchers;

    private RecyclerView recycler_pull_request;
    private RecyclerView.LayoutManager layoutManager;
    private ListPullRequestAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_repository_details);

        Intent intent = getIntent();

        if (intent != null) {
            title_repository = intent.getStringExtra("TITLE_REPOSITORY");
            owner_repository = intent.getStringExtra("OWNER_REPOSITORY");
            open_issues = String.valueOf(intent.getIntExtra("ISSUES_OPEN", 0));
            watchers = String.valueOf(intent.getIntExtra("WATCHER_REPOSITORY", 0));

            TextView open_issues_text = (TextView) findViewById(R.id.open_issues);
            open_issues_text.setText(open_issues += " Issues");

            TextView watchers_txt = (TextView) findViewById(R.id.watchers);
            watchers_txt.setText(watchers += " Watchers");

        }

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
            toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite));

            getSupportActionBar().setTitle(title_repository.toLowerCase());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        recycler_pull_request = (RecyclerView) findViewById(R.id.recycler_list_pull_request);
        recycler_pull_request.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recycler_pull_request.setLayoutManager(layoutManager);


        apiService = ApiClient.getClient().create(GitHubRepositoriesService.class);

        Call<List<GithubPullRequestsResponse>> githubResponse = apiService.listPullRequests(owner_repository, title_repository);

        githubResponse.enqueue(new Callback<List<GithubPullRequestsResponse>>() {

            @Override
            public void onResponse(Call<List<GithubPullRequestsResponse>> call, Response<List<GithubPullRequestsResponse>> response) {

                List<GithubPullRequestsResponse> resultReponse = response.body();

                for (GithubPullRequestsResponse l : resultReponse) {
                    listPullRequests.add(l);
                    adapter = new ListPullRequestAdapter(getBaseContext(), listPullRequests, onClickPullRequest());
                    recycler_pull_request.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<GithubPullRequestsResponse>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    private ListPullRequestAdapter.PullRequestClickListener onClickPullRequest() {
        return new ListPullRequestAdapter.PullRequestClickListener() {

            @Override
            public void onClicPullRequests(View view, int position) {
                GithubPullRequestsResponse g = listPullRequests.get(position);
                Intent intent = new Intent(PullRequestListActivity.this, WebViewPullRequest.class);
                intent.putExtra("URL_PULLREQUEST", g.getHtmlUrl());
                startActivity(intent);
            }
        };
    }
}
