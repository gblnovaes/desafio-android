package net.gabrielnovaes.desafioandroid.services;

import net.gabrielnovaes.desafioandroid.models.GithubPullRequestsResponse;
import net.gabrielnovaes.desafioandroid.models.GithubRepositoriesResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Gabriel on 9/23/16.
 */

public interface GitHubRepositoriesService {

    @GET("search/repositories?q=language:Java&sort=stars&page=")
    Call<GithubRepositoriesResponse> listRepositories(@Query("page") int page);

    @GET("repos/{owner}/{repository}/pulls")
    Call<List<GithubPullRequestsResponse>> listPullRequests(@Path("owner") String owner, @Path("repository") String repository);
}
