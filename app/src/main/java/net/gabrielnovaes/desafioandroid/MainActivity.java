package net.gabrielnovaes.desafioandroid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import net.gabrielnovaes.desafioandroid.adapters.ListRepositoriesAdapter;
import net.gabrielnovaes.desafioandroid.models.GithubRepositoriesResponse;
import net.gabrielnovaes.desafioandroid.models.ListRepositories;
import net.gabrielnovaes.desafioandroid.rest.ApiClient;
import net.gabrielnovaes.desafioandroid.services.EndlessRecyclerViewScrollListener;
import net.gabrielnovaes.desafioandroid.services.GitHubRepositoriesService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "resultado";
    private static Bundle mBundleRecyclerViewState;
    private final String KEY_RECYCLER_STATE = "recycler_state_save";
    public RecyclerView listRepositoriesRecycler;
    List<ListRepositories> listRepositories = new ArrayList<>();

    ProgressDialog progressBar;

    private GitHubRepositoriesService apiService;
    private ListRepositoriesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        }


        progressBar = ProgressDialog.show(this, "Carregando...", "Aguarde", true);


        listRepositoriesRecycler = (RecyclerView) findViewById(R.id.recycler_list_repositories);
        listRepositoriesRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listRepositoriesRecycler.setLayoutManager(linearLayoutManager);

        //initialize data
        loadMoreData(0);

        listRepositoriesRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener((LinearLayoutManager) linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                //Toast.makeText(MainActivity.this, "LoadingPage " + page, Toast.LENGTH_SHORT).show();
                loadMoreData(page);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = listRepositoriesRecycler.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mBundleRecyclerViewState != null) {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            listRepositoriesRecycler.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    public void loadMoreData(int page) {

        apiService = ApiClient.getClient().create(GitHubRepositoriesService.class);

        Call<GithubRepositoriesResponse> githubResponse = apiService.listRepositories(page);

        githubResponse.enqueue(new Callback<GithubRepositoriesResponse>() {
            @Override
            public void onResponse(Call<GithubRepositoriesResponse> call, Response<GithubRepositoriesResponse> response) {

                if (response.isSuccessful()) {

                    progressBar.dismiss();

                    List<ListRepositories> listRep = response.body().getItems();
                    //Log.d(TAG, "onResponse: " + listRep.size());
                    for (ListRepositories l : listRep) {
                        listRepositories.add(l);
                        adapter = new ListRepositoriesAdapter(getBaseContext(), listRepositories, onClickRespository());
                        listRepositoriesRecycler.setAdapter(adapter);
                    }
                } else {
                    progressBar.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GithubRepositoriesResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(MainActivity.this, "Erro ao tentar carregar informações.", Toast.LENGTH_SHORT).show();
                progressBar.dismiss();
            }
        });

    }

    private ListRepositoriesAdapter.RepositoryClickListener onClickRespository() {
        return new ListRepositoriesAdapter.RepositoryClickListener() {
            @Override
            public void onClickRepository(View view, int position) {
                ListRepositories l = listRepositories.get(position);
                Intent intent = new Intent(MainActivity.this, PullRequestListActivity.class);
                intent.putExtra("WATCHER_REPOSITORY", l.getWatchers());
                intent.putExtra("TITLE_REPOSITORY", l.getName());
                intent.putExtra("OWNER_REPOSITORY", l.getOwner().getLogin());
                intent.putExtra("ISSUES_OPEN", l.getOpen_issues());
                startActivity(intent);

            }
        };

    }

}
