package net.gabrielnovaes.desafioandroid.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.gabrielnovaes.desafioandroid.R;
import net.gabrielnovaes.desafioandroid.models.ListRepositories;

import java.util.List;

/**
 * Created by Gabriel on 9/25/16.
 */

public class ListRepositoriesAdapter extends RecyclerView.Adapter<ListRepositoriesAdapter.RepositoriesViewHolder> {

    private Context context;
    private List<ListRepositories> listRepositories;
    private RepositoryClickListener repositoryClickListener;

    public ListRepositoriesAdapter(Context context, List<ListRepositories> listRepositories, RepositoryClickListener repositoryClickListener) {
        this.context = context;
        this.listRepositories = listRepositories;
        this.repositoryClickListener = repositoryClickListener;
    }

    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_repositories, parent, false);
        RepositoriesViewHolder repositoriesViewHolder = new RepositoriesViewHolder(view);
        return repositoriesViewHolder;
    }

    @Override
    public void onBindViewHolder(RepositoriesViewHolder holder, final int position) {
        ListRepositories listRepository = listRepositories.get(position);

        holder.title_repository.setText(listRepository.getName());
        holder.description_repository.setText(listRepository.getDescription());
        holder.forks_count.setText(listRepository.getForks().toString());
        holder.star_count.setText(listRepository.getStargazers_count().toString());
        holder.login.setText(listRepository.getOwner().getLogin());
        holder.full_name.setText(listRepository.getFullName());
        Picasso.with(context).load(listRepository.getOwner().getAvatar_url()).resize(48, 48).centerCrop().placeholder(R.drawable.placeholder_avatar).into(holder.avatar);

        if (repositoryClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    repositoryClickListener.onClickRepository(v, position);
                }
            });
        }
    }


    public interface RepositoryClickListener {
        void onClickRepository(View view, int position);
    }

    @Override
    public int getItemCount() {
        return listRepositories.size();
    }

    public static class RepositoriesViewHolder extends RecyclerView.ViewHolder {

        CardView cardview_list;
        TextView title_repository;
        TextView description_repository;
        TextView star_count;
        TextView forks_count;
        TextView login;
        TextView full_name;
        ImageView avatar;

        public RepositoriesViewHolder(View view) {

            super(view);

            title_repository = (TextView) view.findViewById(R.id.title_repository);
            description_repository = (TextView) view.findViewById(R.id.description_repository);
            star_count = (TextView) view.findViewById(R.id.star_count);
            forks_count = (TextView) view.findViewById(R.id.forks_count);
            login = (TextView) view.findViewById(R.id.login);
            full_name = (TextView) view.findViewById(R.id.full_name);
            cardview_list = (CardView) view.findViewById(R.id.cardview_list);
            avatar = (ImageView) view.findViewById(R.id.avatar);

        }
    }

}
