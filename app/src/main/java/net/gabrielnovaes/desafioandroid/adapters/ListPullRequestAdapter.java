package net.gabrielnovaes.desafioandroid.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.gabrielnovaes.desafioandroid.R;
import net.gabrielnovaes.desafioandroid.models.GithubPullRequestsResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Gabriel on 9/26/16.
 */

public class ListPullRequestAdapter extends RecyclerView.Adapter<ListPullRequestAdapter.PullRequestViewHolder> {
    private Context context;
    private List<GithubPullRequestsResponse> githubPullRequestsResponseList;
    private PullRequestClickListener pullRequestClickListener;

    public ListPullRequestAdapter(Context context, List<GithubPullRequestsResponse> githubPullRequestsResponseList, PullRequestClickListener pullRequestClickListener) {
        this.context = context;
        this.githubPullRequestsResponseList = githubPullRequestsResponseList;
        this.pullRequestClickListener = pullRequestClickListener;
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_pullrequests, parent, false);
        PullRequestViewHolder holder = new PullRequestViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, final int position) {
        GithubPullRequestsResponse githubPullRequestsResponse = githubPullRequestsResponseList.get(position);
        holder.title_pull_request.setText(githubPullRequestsResponse.getTitle());
        holder.description_pull_request.setText(githubPullRequestsResponse.getBody());
        holder.login_pull_request.setText(githubPullRequestsResponse.getUser().getLogin());


        //converting date  in correct format
        Date data_format = githubPullRequestsResponse.getCreatedAt();
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
        String correct_data = "Pull Request Date : " + dt.format(data_format);

        holder.date_pull_request.setText(correct_data);

        Picasso.with(context).load(githubPullRequestsResponse.getUser().getAvatarUrl()).placeholder(R.drawable.placeholder_avatar).resize(48, 48).into(holder.avatar_pull_request);

        if (pullRequestClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pullRequestClickListener.onClicPullRequests(v, position);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return githubPullRequestsResponseList.size();
    }

    public interface PullRequestClickListener {
        void onClicPullRequests(View view, int position);
    }

    public static class PullRequestViewHolder extends RecyclerView.ViewHolder {
        TextView title_pull_request;
        TextView description_pull_request;
        ImageView avatar_pull_request;
        TextView login_pull_request;
        TextView date_pull_request;

        public PullRequestViewHolder(View itemView) {
            super(itemView);
            title_pull_request = (TextView) itemView.findViewById(R.id.title_pull_request);
            description_pull_request = (TextView) itemView.findViewById(R.id.description_pull_request);
            avatar_pull_request = (ImageView) itemView.findViewById(R.id.avatar_pull_request);
            login_pull_request = (TextView) itemView.findViewById(R.id.login_pull_request);
            date_pull_request = (TextView) itemView.findViewById(R.id.date_pull_request);

        }
    }
}



