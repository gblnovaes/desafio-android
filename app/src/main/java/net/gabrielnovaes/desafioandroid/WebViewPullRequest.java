package net.gabrielnovaes.desafioandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebViewPullRequest extends AppCompatActivity {

    private static final String TAG = "resultado";
    private String url_pull_request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_pull_request);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_webview_pull_request);


        Intent intent = getIntent();

        if (intent != null) {
            url_pull_request = intent.getStringExtra("URL_PULLREQUEST");
        }

        Log.d(TAG, "onCreate: " + intent.getStringExtra("URL_PULLREQUEST"));

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
            toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite));

            getSupportActionBar().setTitle("Pull Requests");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        WebView webview = (WebView) findViewById(R.id.webview);
        webview.loadUrl(url_pull_request);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);

    }
}
