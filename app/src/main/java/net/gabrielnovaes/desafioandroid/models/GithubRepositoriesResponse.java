package net.gabrielnovaes.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Gabriel on 9/24/16.
 */

public class GithubRepositoriesResponse {
    @SerializedName("total_count")
    private Integer total_count;

    @SerializedName("incomplete_results")
    private boolean incomplete_results;

    @SerializedName("items")
    private List<ListRepositories> items;

    public Integer getTotal_count() {
        return total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public List<ListRepositories> getItems() {
        return items;
    }


}
