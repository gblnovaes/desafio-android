package net.gabrielnovaes.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gabriel on 9/23/16.
 */

public class ListRepositories {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("owner")
    private OwnerRepository owner;

    @SerializedName("description")
    private String description;

    @SerializedName("forks")
    private Integer forks;

    @SerializedName("stargazers_count")
    private Integer stargazers_count;

    @SerializedName("open_issues")
    private Integer open_issues;


    @SerializedName("watchers")
    private Integer watchers;

    public Integer getWatchers() {
        return watchers;
    }

    public void setWatchers(Integer watchers) {
        this.watchers = watchers;
    }

    public Integer getOpen_issues() {
        return open_issues;
    }

    public void setOpen_issues(Integer open_issues) {
        this.open_issues = open_issues;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public OwnerRepository getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public Integer getForks() {
        return forks;
    }

    public Integer getStargazers_count() {
        return stargazers_count;
    }


}
